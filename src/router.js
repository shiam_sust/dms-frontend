import Vue from 'vue'
import VueRouter from 'vue-router'
import store from './store'

const AppLayout = () => import('@/layouts/App');
const PersonalProjectListLayout = () => import('@/layouts/PersonalProjectList');
const PersonalProjectCreateLayout = () => import('@/layouts/PersonalProjectCreate');
const PersonalProjectPublicCreateLayout = () => import('@/layouts/PersonalProjectPublicCreate');
const TeamPrivateProjectCreateLayout = () => import('@/layouts/TeamPrivateProjectCreate');
const TeamPublicProjectCreateLayout = () => import('@/layouts/TeamPublicProjectCreate');
const TeamProjectListLayout = () => import('@/layouts/TeamProjectList');
const PricingPlanLayout = () => import('@/layouts/PricingPlan');
const ExternalProjectListLayout = () => import('@/layouts/ExternalProjectList');
const ProfileLayout = () => import('@/layouts/Profile');
const AccountStatusLayout = () => import('@/layouts/AccountStatus');



const Signin = () => import('@/views/auth/Signin');
const Signup = () => import('@/views/auth/Signup');
const SignupSecond = () => import('@/views/auth/SignupSecond');
const StripeForm = () => import('@/views/stripe/StripeForm');
const StripeTest = () => import('@/views/stripe/StripeTest');
const ForgotPassword = () => import('@/views/auth/ForgotPassword');
const ForgotEmail = () => import('@/views/auth/ForgotEmail');
const ProfileUpdate = () => import('@/views/profile/UpdateProfile');

const MainDashboard = () => import('@/views/admin/MainDashboard');
const Notification = () => import('@/views/admin/Notification');
const Alert = () => import('@/views/admin/Alert');
const PersonalDashboard = () => import('@/views/admin/PersonalProject/Dashboard');
const PersonalActiveProject = () => import('@/views/admin/PersonalProject/ActiveProject');
const PersonalDraftProject = () => import('@/views/admin/PersonalProject/DraftProject');
const PersonalPastProject = () => import('@/views/admin/PersonalProject/PastProject');
const PersonalDeletedProject = () => import('@/views/admin/PersonalProject/DeletedProject');
const ProjectDetails = () => import('@/views/admin/PersonalProject/ProjectDetails');
const UploadDocument = () => import('@/views/admin/PersonalProject/UploadDocument');


const ExternalPublicProjectDashboard = () => import('@/views/admin/ExternalPublicProjects/Dashboard');
const FindProject = () => import('@/views/admin/ExternalPublicProjects/FindProject');
const ExternalPublicProjectDetails = () => import('@/views/admin/ExternalPublicProjects/Details');
const ExternalPublicGreetings = () => import('@/views/admin/ExternalPublicProjects/Greetings');

const ExternalActiveProject = () => import('@/views/admin/ExternalPublicProjects/ActiveProject');
const ExternalPastProject = () => import('@/views/admin/ExternalPublicProjects/PastProject');
const ExternalDeletedProject = () => import('@/views/admin/ExternalPublicProjects/DeletedProject');


const TeamDashboard = () => import('@/views/admin/Teams/Dashboard');
const CreateNewTeam = () => import('@/views/admin/Teams/CreateNewTeam');
const TeamGreetings = () => import('@/views/admin/Teams/Greetings');
const TeamDetails = () => import('@/views/admin/Teams/TeamDetails');
const SpecificTeamDashboard = () => import('@/views/admin/Teams/TeamDashboard');

const TeamActiveProject = () => import('@/views/admin/TeamProject/ActiveProject');
const TeamDraftProject = () => import('@/views/admin/TeamProject/DraftProject');
const TeamPastProject = () => import('@/views/admin/TeamProject/PastProject');
const TeamDeletedProject = () => import('@/views/admin/TeamProject/DeletedProject');
const TeamProjectDetails = () => import('@/views/admin/TeamProject/ProjectDetails');
const TeamUploadDocument = () => import('@/views/admin/TeamProject/UploadDocument');

//team private project create views
const TeamPrimaryInfo = () => import('@/views/admin/TeamProjectPrivateCreate/PrimaryInfo');
const TeamPrimaryInfoEdit = () => import('@/views/admin/TeamProjectPrivateCreate/PrimaryInfoEdit');
const TeamDeadline = () => import('@/views/admin/TeamProjectPrivateCreate/Deadline');
const TeamCollaborator = () => import('@/views/admin/TeamProjectPrivateCreate/Collaborator');
const TeamAddDocument = () => import('@/views/admin/TeamProjectPrivateCreate/AddDocuments');
const TeamAssignDocument = () => import('@/views/admin/TeamProjectPrivateCreate/AssignDocuments');
const TeamAddSupportStaff = () => import('@/views/admin/TeamProjectPrivateCreate/AddSupportStaffs');
const TeamPricingPlan = () => import('@/views/admin/TeamProjectPrivateCreate/PricingPlan');
const TeamSummary = () => import('@/views/admin/TeamProjectPrivateCreate/Summary');
const TeamStartProject = () => import('@/views/admin/TeamProjectPrivateCreate/StartProject');
const TeamStartGreeting = () => import('@/views/admin/TeamProjectPrivateCreate/StartGreeting');
const TeamDraftGreeting = () => import('@/views/admin/TeamProjectPrivateCreate/DraftGreeting');

//Team project public create views
const TeamPrimaryInfoPublic = () => import('@/views/admin/TeamProjectPublicCreate/PrimaryInfo');
const TeamPrimaryInfoEditPublic = () => import('@/views/admin/TeamProjectPublicCreate/PrimaryInfoEdit');
const TeamCreateDeadlinesPublic = () => import('@/views/admin/TeamProjectPublicCreate/CreateDeadlines');
const TeamCollaboratorPublic = () => import('@/views/admin/TeamProjectPublicCreate/Collaborator');
const TeamAddDocumentsPublic = () => import('@/views/admin/TeamProjectPublicCreate/AddDocuments');
const TeamAddSupportStaffsPublic = () => import('@/views/admin/TeamProjectPublicCreate/AddSupportStaffs');
const TeamPricingPlanPublic = () => import('@/views/admin/TeamProjectPublicCreate/PricingPlan');
const TeamSummaryPublic = () => import('@/views/admin/TeamProjectPublicCreate/Summary');
const TeamStartProjectPublic = () => import('@/views/admin/TeamProjectPublicCreate/StartProject');
const TeamUniquePinPublic = () => import('@/views/admin/TeamProjectPublicCreate/UniquePin');
const TeamDraftGreetingPublic = () => import('@/views/admin/TeamProjectPublicCreate/DraftGreeting');


const PrimaryInfo = () => import('@/views/admin/PersonalProjectPrivateCreate/PrimaryInfo');
const PrimaryInfoEdit = () => import('@/views/admin/PersonalProjectPrivateUpdate/PrimaryInfoEdit');
const Deadline = () => import('@/views/admin/PersonalProjectPrivateCreate/Deadline');
const Collaborator = () => import('@/views/admin/PersonalProjectPrivateCreate/Collaborator');
const AddDocument = () => import('@/views/admin/PersonalProjectPrivateCreate/AddDocuments');
const AssignDocument = () => import('@/views/admin/PersonalProjectPrivateCreate/AssignDocuments');
const AddSupportStaff = () => import('@/views/admin/PersonalProjectPrivateCreate/AddSupportStaffs');
const PricingPlan = () => import('@/views/admin/PersonalProjectPrivateCreate/PricingPlan');
const Summary = () => import('@/views/admin/PersonalProjectPrivateCreate/Summary');
const StartProject = () => import('@/views/admin/PersonalProjectPrivateCreate/StartProject');

//personal project public create
const PrimaryInfoPublic = () => import('@/views/admin/PersonalProjectPublicCreate/PrimaryInfo');
const PrimaryInfoEditPublic = () => import('@/views/admin/PersonalProjectPublicUpdate/PrimaryInfoEdit');
const CreateDeadlinesPublic = () => import('@/views/admin/PersonalProjectPublicCreate/CreateDeadlines');
const CollaboratorPublic = () => import('@/views/admin/PersonalProjectPublicCreate/Collaborator');
const AddDocumentsPublic = () => import('@/views/admin/PersonalProjectPublicCreate/AddDocuments');
const AddSupportStaffsPublic = () => import('@/views/admin/PersonalProjectPublicCreate/AddSupportStaffs');
const PricingPlanPublic = () => import('@/views/admin/PersonalProjectPublicCreate/PricingPlan');
const SummaryPublic = () => import('@/views/admin/PersonalProjectPublicCreate/Summary');
const StartProjectPublic = () => import('@/views/admin/PersonalProjectPublicCreate/StartProject');
const UniquePinPublic = () => import('@/views/admin/PersonalProjectPublicCreate/UniquePin');
const DraftGreetingPublic = () => import('@/views/admin/PersonalProjectPublicCreate/DraftGreeting');

//pricing plans
const PricingPlansPersonal = () => import('@/views/admin/PricingPlans/PricingPlansPersonal');
const PricingPlansTeam = () => import('@/views/admin/PricingPlans/PricingPlansTeam');
const DefaultPricingPlans = () => import('@/views/admin/PricingPlans/DefaultPricingPlans');
const CreditCard = () => import('@/views/admin/PricingPlans/CreditCard');
const SavedCreditCard = () => import('@/views/admin/PricingPlans/SavedCreditCard');
const DefaultGreeting = () => import('@/views/admin/PricingPlans/DefaultGreeting');
const StopPaymentTeam = () => import('@/views/admin/PricingPlans/StopPaymentTeam');
const StopPaymentTeamConfirm = () => import('@/views/admin/PricingPlans/StopPaymentTeamConfirm');
const StopPaymentPersonal = () => import('@/views/admin/PricingPlans/StopPaymentPersonal');
const StopPaymentPersonalConfirm = () => import('@/views/admin/PricingPlans/StopPaymentPersonalConfirm');
const ChangePlansPersonal = () => import('@/views/admin/PricingPlans/ChangePlansPersonal');
const ChangePlansPersonalExecute = () => import('@/views/admin/PricingPlans/ChangePlansPersonalExecute');
const ChangePlansTeam = () => import('@/views/admin/PricingPlans/ChangePlansTeam');
const ChangePlansTeamExecute = () => import('@/views/admin/PricingPlans/ChangePlansTeamExecute');

//profile
const ProfileCreate = () => import('@/views/admin/Profile/ProfileCreate');
const UsernameCreate = () => import('@/views/admin/Profile/UsernameCreate');
const UsernameConfirm = () => import('@/views/admin/Profile/UsernameConfirm');
const PasswordCreate = () => import('@/views/admin/Profile/PasswordCreate');
const PasswordConfirm = () => import('@/views/admin/Profile/PasswordConfirm');
const DocumentsCreate = () => import('@/views/admin/Profile/DocumentsCreate');
const ProfileGreeting = () => import('@/views/admin/Profile/ProfileGreeting');

//AccountStatus
const AccountStatusSpaceUsed = () => import('@/views/admin/AccountStatus/SpaceUsed');
const AccountStatusTeams = () => import('@/views/admin/AccountStatus/Teams');
const AccountStatusPersonal = () => import('@/views/admin/AccountStatus/Personal');
const AccountStatusExternal = () => import('@/views/admin/AccountStatus/ExternalPublicProjects');


Vue.use(VueRouter)

const Router = new VueRouter({
    mode: 'history',
    linkActiveClass: "active",
    linkExactActiveClass: "exact-active",
    routes: [

        {
            path: '',
            component: AppLayout,
            meta: { auth: true },
            children: [
                { path: '/main-dashboard', component: MainDashboard, name:'MainDashboard' },
                { path: '/notification', component: Notification, name:'Notification' },
                { path: '/alert', component: Alert, name:'Alert' },
                { path: '/personal-project/dashboard', component: PersonalDashboard, name:'PersonalProjectDashboard' },
                { path: '/external-public-project/dashboard', component: ExternalPublicProjectDashboard, name:'ExternalDashboard' },
                { path: '/external-public-project/find-project', component: FindProject, name:'ExternalFindProject' },
                { path: '/external-public-project/:pid/project-details', component: ExternalPublicProjectDetails, name:'ExternalProjectDetails' },
                { path: '/external-public-project/greetings', component: ExternalPublicGreetings, name:'ExternalGreetings' },
                { path: '/team/dashboard', component: TeamDashboard, name:'TeamDashboard' },
                { path: '/team/new-team', component: CreateNewTeam, name:'NewTeam' },
                { path: '/team/greetings', component: TeamGreetings, name:'TeamGreetings' },
                { path: '/team/:id/details', component: TeamDetails, name:'TeamDetails' },
                { path: '/team/:tname/:id/dashboard', component: SpecificTeamDashboard, name:'SpecificTeamDashboard' },
                { path: '/pricing/default-greeting', component: DefaultGreeting, name: 'PricingDefaultGreeting' },
                { path: '/team/:id/dashboard', component: SpecificTeamDashboard, name:'TeamDashboardById' },

            ],
        },

        {
            path: '',
            component: ExternalProjectListLayout,
            meta: { auth: true },
            children: [
                { path: '/external-project/active-projects', component: ExternalActiveProject, name: 'ExternalActiveProjects' },
                { path: '/external-project/past-projects', component: ExternalPastProject, name: 'ExternalPastProjects' },
                { path: '/external-project/deleted-projects', component: ExternalDeletedProject, name: 'ExternalDeletedProjects' },
            ],
        },

        {
            path: '',
            component: PersonalProjectListLayout,
            meta: { auth: true },
            children: [
                { path: '/personal-project/active-projects', component: PersonalActiveProject, name: 'PersonalActiveProjects' },
                { path: '/personal-project/draft-projects', component: PersonalDraftProject, name: 'PersonalDraftProjects' },
                { path: '/personal-project/past-projects', component: PersonalPastProject, name: 'PersonalPastProjects' },
                { path: '/personal-project/deleted-projects', component: PersonalDeletedProject, name: 'PersonalDeletedProjects' },
                { path: '/personal-project/:id/details', component: ProjectDetails, name: 'PersonalProjectDetails' },
                { path: '/personal-project/:id/upload-documents', component: UploadDocument, name: 'PersonalProjectUploadDocument'},
            ],
        },

        {
            path: '',
            component: PersonalProjectCreateLayout,
            meta: { auth: true },
            children: [
                { path: '/personal-project/create', component: PrimaryInfo, name: 'PersonalCreatePrimaryInfo' },
                { path: '/personal-project/:id/edit', component: PrimaryInfoEdit, name: 'PersonalUpdatePrimaryInfo' },
                { path: '/personal-project/:id/set-deadlines', component: Deadline, name: 'PersonalCreateDeadline' },
                { path: '/personal-project/:id/add-collaborators', component: Collaborator, name: 'PersonalCreateCollaborator' },
                { path: '/personal-project/:id/add-documents', component: AddDocument, name: 'PersonalCreateAddDocument' },
                { path: '/personal-project/:id/assign-documents', component: AssignDocument, name: 'PersonalCreateAssignDocument' },
                { path: '/personal-project/:id/add-support-staff', component: AddSupportStaff, name: 'PersonalCreateAddSupportStaff' },
                { path: '/personal-project/:id/plans', component: PricingPlan, name: 'PersonalCreatePricingPlan' },
                { path: '/personal-project/:id/summary', component: Summary, name: 'PersonalCreateSummary' },
                { path: '/personal-project/:id/start', component: StartProject, name: 'PersonalCreateStartProject' },
            ],
        },

        {
            path: '',
            component: PersonalProjectPublicCreateLayout,
            meta: { auth: true },
            children: [
                { path: '/personal-project/public/create', component: PrimaryInfoPublic, name: 'PPPublicCreatePrimaryInfo' },
                { path: '/personal-project/public/:id/edit', component: PrimaryInfoEditPublic, name: 'PPPublicCreatePrimaryInfoEdit' },
                { path: '/personal-project/public/:id/set-deadlines', component: CreateDeadlinesPublic, name: 'PPPublicCreateSetDeadlines' },
                { path: '/personal-project/public/:id/collaborators', component: CollaboratorPublic, name: 'PPPublicCreateCollaborator' },
                { path: '/personal-project/public/:id/add-documents', component: AddDocumentsPublic, name: 'PPPublicCreateAddDocuments' },
                { path: '/personal-project/public/:id/add-support-staffs', component: AddSupportStaffsPublic, name: 'PPPublicCreateAddSupportStaffs' },
                { path: '/personal-project/public/:id/plans', component: PricingPlanPublic, name: 'PPPublicCreatePricingPlan' },
                { path: '/personal-project/public/:id/summary', component: SummaryPublic, name: 'PPPublicCreateSummary' },
                { path: '/personal-project/public/:id/start', component: StartProjectPublic, name: 'PPPublicCreateStartProject' },
                { path: '/personal-project/public/:id/pid', component: UniquePinPublic, name: 'PPPublicUniquePinPublic' },
                { path: '/personal-project/public/draft', component: DraftGreetingPublic, name: 'PPPublicDraftGreetingPublic' },
            ],
        },

        //team project list routes
        {
            path: '',
            component: TeamProjectListLayout,
            meta: { auth: true },
            children: [
                { path: '/team-project/:teamId/active-projects', component: TeamActiveProject, name: 'TeamActiveProjects' },
                { path: '/team-project/:teamId/draft-projects', component: TeamDraftProject, name: 'TeamDraftProjects' },
                { path: '/team-project/:teamId/past-projects', component: TeamPastProject, name: 'TeamPastProjects' },
                { path: '/team-project/:teamId/deleted-projects', component: TeamDeletedProject, name: 'TeamDeletedProjects' },
                { path: '/team-project/:teamId/project/:id/details', component: TeamProjectDetails, name: 'TeamProjectDetails' },
                { path: '/team-project/:teamId/project/:id/upload-documents', component: TeamUploadDocument, name: 'TeamUploadDocuments' },
            ],
        },

        //team private project create related routes
        {
            path: '',
            component: TeamPrivateProjectCreateLayout,
            meta: { auth: true },
            children: [
                { path: '/team-project/:teamId/projects/create', component: TeamPrimaryInfo, name: 'TeamCreatePrimaryInfo' },
                { path: '/team-project/:teamId/projects/:id/edit', component: TeamPrimaryInfoEdit, name: 'TeamUpdatePrimaryInfo' },
                { path: '/team-project/:teamId/projects/:id/set-deadlines', component: TeamDeadline, name: 'TeamCreateDeadline' },
                { path: '/team-project/:teamId/projects/:id/add-collaborators', component: TeamCollaborator, name: 'TeamCreateCollaborator' },
                { path: '/team-project/:teamId/projects/:id/add-documents', component: TeamAddDocument, name: 'TeamCreateAddDocument' },
                { path: '/team-project/:teamId/projects/:id/assign-documents', component: TeamAssignDocument, name: 'TeamCreateAssignDocument' },
                { path: '/team-project/:teamId/projects/:id/add-support-staff', component: TeamAddSupportStaff, name: 'TeamCreateAddSupportStaff' },
                { path: '/team-project/:teamId/projects/:id/plans', component: TeamPricingPlan, name: 'TeamCreatePricingPlan' },
                { path: '/team-project/:teamId/projects/:id/summary', component: TeamSummary, name: 'TeamCreateSummary' },
                { path: '/team-project/:teamId/projects/:id/start', component: TeamStartProject, name: 'TeamCreateStartProject' },
                { path: '/team-project/:teamId/projects/:id/start-greeting', component: TeamStartGreeting, name: 'TeamProjectStartGreeting' },
                { path: '/team-project/:teamId/projects/:id/draft-greeting', component: TeamDraftGreeting, name: 'TeamProjectDraftGreeting' },
            ],
        },

        {
            path: '',
            component: TeamPublicProjectCreateLayout,
            meta: { auth: true },
            children: [
                { path: '/team-project/:teamId/public/projects/create', component: TeamPrimaryInfoPublic, name: 'TeamPublicPrimaryInfo' },
                { path: '/team-project/:teamId/public/projects/:id/edit', component: TeamPrimaryInfoEditPublic, name: 'TeamPublicPrimaryInfoEdit' },
                { path: '/team-project/:teamId/public/projects/:id/set-deadlines', component: TeamCreateDeadlinesPublic, name: 'TeamPublicSetDeadlines' },
                { path: '/team-project/:teamId/public/projects/:id/collaborators', component: TeamCollaboratorPublic, name: 'TeamPublicCollaborator' },
                { path: '/team-project/:teamId/public/projects/:id/add-documents', component: TeamAddDocumentsPublic, name: 'TeamPublicAddDocuments' },
                { path: '/team-project/:teamId/public/projects/:id/add-support-staffs', component: TeamAddSupportStaffsPublic, name: 'TeamPublicAddSupportStaffs' },
                { path: '/team-project/:teamId/public/projects/:id/plans', component: TeamPricingPlanPublic, name: 'TeamPublicPricingPlan' },
                { path: '/team-project/:teamId/public/projects/:id/summary', component: TeamSummaryPublic, name: 'TeamPublicSummary' },
                { path: '/team-project/:teamId/public/projects/:id/start', component: TeamStartProjectPublic, name: 'TeamPublicStartProject' },
                { path: '/team-project/:teamId/public/projects/:id/pid', component: TeamUniquePinPublic, name: 'TeamPublicUniquePin' },
                { path: '/team-project/:teamId/public/projects/:id/draft', component: TeamDraftGreetingPublic, name: 'TeamPublicDraftGreeting' },
            ],
        },

        {
            path: '',
            component: PricingPlanLayout,
            meta: { auth: true },
            children: [
                { path: '/pricing/plans/personal', component: PricingPlansPersonal, name: 'PricingPlansPersonal' },
                { path: '/pricing/plans/team', component: PricingPlansTeam, name: 'PricingPlansTeam' },
                { path: '/pricing/plans/default-plans', component: DefaultPricingPlans, name: 'PricingDefaultPlans' },
                { path: '/pricing/credit-card', component: CreditCard, name: 'PricingCreditCard' },
                { path: '/pricing/credit-card/saved', component: SavedCreditCard, name: 'PricingSavedCreditCard' },
                { path: '/pricing/stop-payment/team', component: StopPaymentTeam, name: 'PricingStopPaymentTeam' },
                { path: '/pricing/stop-payment/personal', component: StopPaymentPersonal, name: 'PricingStopPaymentPersonal' },
                { path: '/pricing/stop-payment/team/:id/confirm', component: StopPaymentTeamConfirm, name: 'PricingStopPaymentTeamConfirm' },
                { path: '/pricing/stop-payment/personal/confirm', component: StopPaymentPersonalConfirm, name: 'PricingStopPaymentPersonalConfirm' },
                { path: '/pricing/change-plans/personal', component: ChangePlansPersonal, name: 'PricingChangePlansPersonal' },
                { path: '/pricing/change-plans/personal/execute', component: ChangePlansPersonalExecute, name: 'PricingChangePlansPersonalExecute' },
                { path: '/pricing/change-plans/team', component: ChangePlansTeam, name: 'PricingChangePlansTeam' },
                { path: '/pricing/change-plans/team/:id/execute', component: ChangePlansTeamExecute, name: 'PricingChangePlansTeamExecute' },
            ],
        },

        {
            path: '',
            component: ProfileLayout,
            meta: { auth: true },
            children: [
                { path: '/profile/create', component: ProfileCreate, name: 'ProfileCreate' },
                { path: '/profile/username/create', component: UsernameCreate, name: 'ProfileUsernameCreate' },
                { path: '/profile/username/create/confirm/:email', component: UsernameConfirm, name: 'ProfileUsernameCreateConfirm' },
                { path: '/profile/password/create', component: PasswordCreate, name: 'ProfilePasswordCreate' },
                { path: '/profile/password/create/confirm', component: PasswordConfirm, name: 'ProfilePasswordCreateConfirm' },
                { path: '/profile/documents/create', component: DocumentsCreate, name: 'ProfileDocumentsCreate' },
                { path: '/profile/greeting', component: ProfileGreeting, name: 'ProfileGreeting' },
            ],
        },

        {
            path: '',
            component: AccountStatusLayout,
            meta: { auth: true },
            children: [
                { path: '/account-status/space-used', component: AccountStatusSpaceUsed, name: 'AccountStatusSpaceUsed' },
                { path: '/account-status/teams', component: AccountStatusTeams, name: 'AccountStatusTeams' },
                { path: '/account-status/personal', component: AccountStatusPersonal, name: 'AccountStatusPersonal' },
                { path: '/account-status/external-public-projects', component: AccountStatusExternal, name: 'AccountStatusExternal' },
                
            ],
        },

        { path: '/signin', component: Signin},
        { path: '/signup', component: Signup},
        { path: '/signup-second/:email', component: SignupSecond},
        { path: '/checkout', component: StripeForm},
        { path: '/checkout-test', component: StripeTest},
        { path: '/profile-update', component: ProfileUpdate},
        { path: '/forgot-password', component: ForgotPassword},
        { path: '/forgot-email', component: ForgotEmail},

    ]
});

Router.beforeEach((to, from, next) => {
    if (to.matched.some(record => record.meta.auth) && !store.getters['auth/isLoggedIn']) {
        next('/signin')
        return
    }
    next()
});

export default Router;
