import axios from 'axios'

// Initial state set here
const getDefaultState = () => {
    return {
        notifications: [],
        alerts: [],
        searchedProjects: []
    }
}

const NotificationModule = {
    namespaced: true,

    state: getDefaultState(),

    getters: {
        notifications(state) {
            return state.notifications
        },
        alerts(state) {
            return state.alerts
        },
        searchedProjects(state) {
            return state.searchedProjects
        }
    },

    mutations: {
        SET_NOTIFICATIONS(state, data) {
            state.notifications = data
        },
        SET_ALERTS(state, data) {
            state.alerts = data
        },
        SET_SEARCHED_PROJECTS(state, data) {
            state.searchedProjects = data
        },
        RESET_STATE (state) {
            Object.assign(state, getDefaultState())
        }
    },

    actions: {
        getNotifications({ commit }) {
            return axios.
                get('notifications')
                .then((res) => {                    
                    commit('SET_NOTIFICATIONS', res.data.data);
                })
        },
        getSearchedData({ commit }, title) {
            return axios.
                get(`search?q=${title}`)
                .then((res) => {                    
                    commit('SET_SEARCHED_PROJECTS', res.data.data);
                    return res;
                })
        },
        getAlerts({ commit }) {
            return axios.
                get('alerts')
                .then((res) => {                    
                    commit('SET_ALERTS', res.data.data);
                })
        },
        markReadNotification() {
            return axios
                .post(`mark-as-read-noti`)
                .then((res) => {                                              
                    return res;
                })
        }, 
        countNotification() {
            return axios.
                get('notification-count')
                .then((res) => {                    
                    return res; 
                })
        },
    }
}

export default NotificationModule
