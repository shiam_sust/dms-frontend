import axios from 'axios'

// Initial state set here
const getDefaultState = () => {
    return {
        temp: false,
        documentCreated: false,
        documentAssigned: false,
        project: {collaborators:[]},
        documents: [],
        collaborators: [],
    }
  }

const TeamPublicProjectModule = {
    namespaced: true,

    state: getDefaultState(),

    getters: {
        project(state) {
            return state.project
        },
        documents(state) {            
            // return state.documents.map((item)=> {
            //     item.collaborators_ids = [];
            //     return item;
            // });
            return state.documents;
        },
        collaborators(state) {
            return state.collaborators;
        }
    },

    mutations: {
        SET_BOOL(state, data) {
            state.temp = data;
        }, 
        SET_PROJECT(state, data) {
            state.project = data
        }, 
        SET_DOCUMENTS(state, data) {
            state.documents = data
        }, 
        SET_COLLABORATORS(state, data) {
            state.collaborators = data
        }, 
        DOCUMENTS_CREATED(state, data) {
            state.documentCreated = data
        }, 
        DOCUMENTS_ASSIGNED(state, data) {
            state.documentAssigned = data
        }, 
        UPDATE_PROJECT_COLLABORATOR(state, {data, collaboratorId}) {                       
            state.project.collaborators.map((item)=> {
                if(item.id === collaboratorId) {
                    item.documents = data;
                    item.expanded = true;
                } else {
                    item.expanded = false;    
                    item.documents = [];
                }
                return item;
            })            
        },
        COLLAPSE_COLLABORATOR_DOCUMENT(state, collaboratorId) {                       
            state.project.collaborators.map((item)=> {
                if(item.id === collaboratorId) {                    
                    item.expanded = false;
                    item.documents = [];
                } 
                return item;
            })            
        },
        REMOVE_COLLABORATOR(state, collaboratorId) {
            const i = state.collaborators.map(item => item.id).indexOf(collaboratorId);
            state.collaborators.splice(i, 1);    
        },   
        RESET_STATE (state) {
            Object.assign(state, getDefaultState())
        }     
    },

    actions: {
        storePrimaryInfo({ commit }, data, config) {
            return axios
                .post('/team/public/create', data, config)
                .then((res) => {                                              
                    commit('SET_PROJECT', res.data.data);     
                    return res;                  
                })
        },
        updatePrimaryInfo({ commit }, {formData, projectId}, config) {
            return axios
                .post(`/team/public/${projectId}/update`, formData, config)
                .then((res) => {                                              
                    commit('SET_PROJECT', res.data.data);     
                    return res;                  
                })
        },
        getPrimaryInfo({ commit }, projectId) {
            return axios
                .get(`/team/public/${projectId}/primary-info`)
                .then((res) => {                                              
                    commit('SET_PROJECT', res.data.data);     
                    return res;                  
                })
        },
        storeDeadlines({ commit }, data) {
            return axios
                .put(`/team/public/${data.id}/set-deadline`, data.payload)
                .then((res) => {                                                  
                    commit('SET_PROJECT', res.data.data);    
                    return res;                   
                })
        },
        storeTotalCollaborators({ commit }, {total_collaborators, projectId}) {           
            return axios
                .post(`/team/public/${projectId}/total-collaborators`, {total_collaborators:total_collaborators})
                .then((res) => {                                                  
                    commit('SET_PROJECT', res.data.data);    
                    return res;                   
                })
        },
        getDocuments({ commit }, projectId) {           
            return axios
                .get(`/team/public/${projectId}/documents`)
                .then((res) => {                                                  
                    commit('SET_DOCUMENTS', res.data.data);    
                    return res;                   
                })
        },
        storeDocuments({ commit }, {formData, projectId}, config) {           
            return axios
                .post(`/team/public/${projectId}/documents`, formData, config)
                .then((res) => {                                                  
                    commit('DOCUMENTS_CREATED', true);    
                    return res;                   
                })
        },
        removeDocument(context, {projectId, documentId}) {
            return axios
                .delete(`/team/public/${projectId}/documents/${documentId}`)
                .then((res) => {                          
                    return res;
                })
        },     
        removeDocumentTemplate(context, {projectId, documentId}) {
            return axios
                .delete(`/team/public/${projectId}/documents/${documentId}/template`)
                .then((res) => {                          
                    return res;
                })
        }, 
        getSupportStaffs(context, projectId) {           
            return axios
                .get(`/team/public/${projectId}/support-staffs`)
                .then((res) => {                        
                    return res;                   
                })
        }, 
        storeSupportStaffs({ commit }, {supportStaffs, projectId}) {           
            return axios
                .post(`/team/public/${projectId}/support-staffs`, {supportStaffs:supportStaffs})
                .then((res) => {                                                  
                    commit('SET_PROJECT', res.data.data);    
                    return res;                   
                })
        },
        removeUser({ commit }, {projectId, userId, userType}) {
            return axios
                .delete(`/team/public/${projectId}/users/${userId}?user_type=${userType}`)
                .then((res) => {  
                    if(userType === 'COLLABORATOR') {
                        commit(`REMOVE_${userType}`, userId);
                    }           
                    return res;
                })
        },
        startProject(context, projectId) {           
            return axios
                .post(`/team/public/${projectId}/start-project`, {})
                .then((res) => {                    
                    return res;                   
                })
        },
        deleteAttachment({ commit }, data) {
            return axios
                .delete(`/team/public/${data.project_id}/attachments/${data.attachment_id}`)
                .then((res) => {                                              
                    commit('SET_BOOL', res.data.data);     
                    return res;
                })
        }, 
        
    }
}

export default TeamPublicProjectModule
