import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from "vuex-persistedstate";

import AuthModule from './modules/auth.js'
import StripeModule from './modules/stripes.js'
import PrivateProjectModule from './modules/privateProject.js'
import PublicProjectModule from './modules/publicProject.js'
import ProjectListModule from './modules/projectLists.js'
import SnackbarModule from './modules/snackbar.js'
import TeamModule from './modules/teams.js'
import TeamPrivateProjectModule from './modules/teamPrivateProject.js'
import TeamPublicProjectModule from './modules/teamPublicProject.js'
import TeamProjectListModule from './modules/teamProjectLists.js'
import NotificationModule from './modules/notifications.js'
import PricingPlansModule from './modules/pricingPlans.js'
import ProfilesModule from './modules/profiles.js'

Vue.use(Vuex)

export default new Vuex.Store({
    modules: {
        auth: AuthModule,
        stripes: StripeModule,
        privateProjects: PrivateProjectModule,
        publicProjects: PublicProjectModule,
        projectLists: ProjectListModule,
        snackbar: SnackbarModule,
        teams: TeamModule,
        teamPrivateProjects: TeamPrivateProjectModule,
        teamPublicProjects: TeamPublicProjectModule,
        teamProjectLists: TeamProjectListModule,
        notifications: NotificationModule,
        pricingPlans: PricingPlansModule,
        profiles: ProfilesModule,
    },
    plugins: [
        createPersistedState(),
    ]
})